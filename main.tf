terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.8.0"
    }
  }

  backend "http" {
  }
}

locals {
  grenoble_ia1_p7 = {
    principal_id = "a0c131e3-e748-461f-89a4-b344a5ed68fd"
  }
  pbayart = {
    principal_id = "cd600bde-71ef-455c-b682-53605adc405e"
  }
  rclement = {
    principal_id = "01f5bbf6-e4aa-4d76-988e-f85e88b0f83f"
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

data "azurerm_subscription" "subscription" {}

resource "azurerm_resource_group" "default" {
  name     = "p7-activite-spark"
  location = "France Central"
}

resource "azurerm_databricks_workspace" "default" {
  name                = "databricks-ws"
  resource_group_name = azurerm_resource_group.default.name
  location            = azurerm_resource_group.default.location
  sku                 = "trial"
}
