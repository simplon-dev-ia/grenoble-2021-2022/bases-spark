![Spark Logo](spark.png)

# Bases Spark

> Introduction à l'utilisation de Spark

Nous allons découvrir l'utilisation du moteur d'exécution [Spark](https://spark.apache.org)
via le langage Python.

## Recherche d'information

- [ ] Quels problèmes l'utilisation de Spark permet-elle de résoudre ?
- [ ] Comment fonctionne Spark dans les grandes lignes ?
- [ ] Dans quel langage de programmation est développé Spark ?
- [ ] Comment utiliser Spark en Python ?
- [ ] Quelles sont les différences entre un DataFrame Spark et un DataFrame Pandas ?
- [ ] Quelles sont les alternatives à l'utilisation de Spark ?
- [ ] Quel est le lien entre Spark et Databricks ?

## Mise en place en local

L'utilisation de Spark en Python repose sur la bibliothèque [`pyspark`](https://spark.apache.org/docs/latest/api/python/).

Pour simplifier l'utilisation de Spark en Python, nous allons utiliser une image
Docker déjà préparée par la communauté Jupyter contenant JupyterLab + PySpark + Spark :

- https://hub.docker.com/r/jupyter/pyspark-notebook
- https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-pyspark-notebook
- https://jupyter-docker-stacks.readthedocs.io/en/latest/using/specifics.html#apache-spark

Pour instantier cette image en local avec la persistence des notebooks :

```bash
docker run --rm \
    -v "${PWD}/notebooks:/home/jovyan/notebooks" \
    -p "8888:8888" \
    -p "4040:4040" \
    jupyter/pyspark-notebook:latest
```

Ouvrir JupyterLab avec le lien dans les logs du conteneur, de la forme "http://127.0.0.1:8888/lab?token=..."

## Mise en place sur Azure Databricks

Pour exécuter du code Spark sur [Azure Databricks](https://azure.microsoft.com/en-us/services/databricks/) :

1. Créer une ressource [Azure Databricks Workspace](https://portal.azure.com/#blade/HubsExtension/BrowseResourceBlade/resourceType/Microsoft.Databricks%2Fworkspaces)
    - Sélectionner le plan désiré (utiliser "Trial" si encore disponible sur l'organisation)
2. Entrer dans le workspace Databricks
3. Créer un cluster de calcul dans le menu "Compute"
    - Choisir un nom pour le cluster
    - Choisir le mode : "Single node" pour du mono-noeud, "Standard" pour du multi-noeud
    - Choisir le runtime : dernière version LTS disponible
    - Autoscaling : désactiver si pas besoin de charge de calcul élastique
    - Terminate : après 120 secondes d'inactivité
    - Choisir le type de worker : exemple "Standard_DS3_v2"
    - Choisir le nombre de worker : minimum 2 pour du multi-noeud
    - Choisir le type de driver : exemple "Standard_F4"
4. Importer des données dans le menu "Data"
    - Cliquer sur "Create Table"
    - Uploader un ou plusieurs jeux de données (CSV, etc.) dans l'encart "Files"
    - Cliquer sur "Create" et suivre les instructions
5. Importer un notebook dans le menu "Workspace"
    - Sélectionner le dossier "Users" / "<monusername@mondomain.com>"
    - Dans le panneau de droite, cliquer sur la flèche et sélectionner "Import"
    - Uploader un ou plusieurs notebooks dans l'encart d'upload
    - Cliquer sur "Import"
6. Ouvrir le notebook uploadé dans le menu "Workspace"
7. Dans le notebook :
    - Choisir un cluster d'exécution en haut à gauche (marqué "Detached" par défaut)
    - Dans une cellule, trouver le chemin vers le fichier du jeu de données avec la commande: `display(dbutils.fs.ls("/FileStore/tables/"))`
    - Copier le chemin du jeu de données dans la cellule du notebook adéquate : exemple, `dbfs:/FileStore/tables/IMDB_Dataset.csv`
    - Exécuter le notebook depuis le début normalement
8. Une fois les calculs terminés, bien penser à détacher les notebooks du cluster et à terminer le cluster dans le menu "Compute"
